import React from 'react';
import Button from "../components/Button/Button";
import Modal from "../components/Modal/Modal";
import Title from "../components/Title/Title";
import style from './App.module.scss'

export default class App extends React.Component {
    state = {
        firstModalActive: false,
        secondModalActive: false
    }

    firstModal = () => {
        this.setState({
            firstModalActive: !this.state.firstModalActive
        })
    }

    secondModal = () => {
        this.setState({
            secondModalActive: !this.state.secondModalActive
        })
    }

    render() {
        return (
            <div className={style.container}>
                <Title text={"Hello React!!!"}/>
                <div className={style.buttonGroup}>
                    <Button
                        onClick={this.firstModal}
                        backgroundColor={"rgb(255, 156, 156)"}
                        text={"Open first modal"}
                    />

                    <Button
                        onClick={this.secondModal}
                        backgroundColor={"rgb(170 255 186)"}
                        text={"Open second modal"}
                    />
                </div>
                {this.state.firstModalActive &&
                <Modal
                    header={'First modal'}
                    closeButton={true}
                    isOpen={this.firstModal}
                    textmodal={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum explicabo ipsam iste magnam officiis quae?"}
                    actions={
                        <div className={style.modalFooter}>
                            <Button text={"Ok"}/>
                            <Button text={"Cancel"} onClick={this.firstModal}/>
                        </div>}
                />}
                {this.state.secondModalActive &&
                <Modal
                    header={'Second modal'}
                    closeButton={false}
                    isOpen={this.secondModal}
                    textmodal={"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet aperiam, dolores ea eaque error exercitationem expedita hic nam natus nisi non omnis quae quas, suscipit voluptatem voluptatibus voluptatum? Provident."}
                    actions={
                        <div className={style.modalFooter}>
                            <Button text={"React"}/>
                            <Button text={"Angular"}/>
                            <Button text={"Cancel"} onClick={this.secondModal}/>
                        </div>}
                />}
            </div>
        );
    }
}

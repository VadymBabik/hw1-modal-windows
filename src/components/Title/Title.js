import React from 'react';
import styles from './Title.module.scss'

export default class Title extends React.Component {
    render() {
        return (
            <h1 className={styles.titleText}>
                {this.props.text}
            </h1>
        )
    }
}

import React from 'react';
import styles from './Modal.module.scss'

export default class Modal extends React.Component {
    render() {
        return (
            <div
                onClick={this.props.isOpen}
                className={styles.modal}>
                <div onClick={event => event.stopPropagation()}
                     className={styles.modal__content}>
                    <div className={styles.modal__header}>
                        <h5 className={styles.modal__title}>{this.props.header}</h5>
                        {this.props.closeButton && <span onClick={this.props.isOpen}>&#10761;</span>}
                    </div>
                    <div className={styles.modal__body}>
                        <p className={styles.body__text}>{this.props.textmodal}</p>
                    </div>
                    {this.props.actions}
                </div>
            </div>
        )
    }
}